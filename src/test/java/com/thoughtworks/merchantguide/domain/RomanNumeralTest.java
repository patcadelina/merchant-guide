package com.thoughtworks.merchantguide.domain;
import static org.hamcrest.Matchers.hasEntry;

import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class RomanNumeralTest {
	@Test
	public void shouldBuildMap() {
		Map<Character, Integer> map = RomanNumeral.buildMap();
		Assert.assertThat(map, hasEntry('I', 1));
		Assert.assertThat(map, hasEntry('V', 5));
		Assert.assertThat(map, hasEntry('X', 10));
		Assert.assertThat(map, hasEntry('L', 50));
		Assert.assertThat(map, hasEntry('C', 100));
		Assert.assertThat(map, hasEntry('D', 500));
		Assert.assertThat(map, hasEntry('M', 1000));
	}
}
