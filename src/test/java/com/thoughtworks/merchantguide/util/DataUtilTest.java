package com.thoughtworks.merchantguide.util;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.thoughtworks.merchantguide.domain.Coin;
import com.thoughtworks.merchantguide.domain.Denomination;
import com.thoughtworks.merchantguide.domain.Query;
import com.thoughtworks.merchantguide.domain.QueryType;
import com.thoughtworks.merchantguide.domain.Unit;
import com.thoughtworks.merchantguide.exception.InvalidUnitException;

public class DataUtilTest {
	List<String> data = new ArrayList<>();

	@Before
	public void init() {
		data.add("piso is I");
		data.add("lima is V");
		data.add("sampu is X");
		data.add("piso piso Silver is 50 Credits");
		data.add("piso lima Gold is 200 Credits");
		data.add("sampu lima Iron is 90 Credits");
		data.add("how much is sampu sampu piso piso lima ?");
		data.add("how many Credits is piso sampu Silver ?");
		data.add("how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
	}

	@Test
	public void shouldFilterUnits() throws InvalidUnitException {
		Map<String, Unit> map = DataUtil.filterUnits(data);

		Assert.assertThat(map.size(), is(3));
		Assert.assertThat(map, hasEntry("piso", Unit.newInstance("piso", 'I')));
		Assert.assertThat(map, hasEntry("lima", Unit.newInstance("lima", 'V')));
		Assert.assertThat(map, hasEntry("sampu", Unit.newInstance("sampu", 'X')));
	}

	@Test
	public void shouldFilterCredits() throws InvalidUnitException {
		Map<String, Unit> unitMap = DataUtil.filterUnits(data);
		Map<String, Denomination> denominationMap = DataUtil.filterDenomination(data, unitMap);

		Assert.assertThat(denominationMap.size(), is(3));
		Assert.assertThat(denominationMap, hasEntry("Silver", Denomination.newInstance(Coin.Silver, 2, 50)));
		Assert.assertThat(denominationMap, hasEntry("Gold", Denomination.newInstance(Coin.Gold, 4, 200)));
		Assert.assertThat(denominationMap, hasEntry("Iron", Denomination.newInstance(Coin.Iron, 15, 90)));
	}

	@Test
	public void shouldFilterQueries() throws InvalidUnitException {
		List<Query> actual = DataUtil.filterQueries(data);
		List<Query> expected = new ArrayList<>();
		expected.add(Query.newInstance(QueryType.COMPUTE, "sampu sampu piso piso lima"));
		expected.add(Query.newInstance(QueryType.CONVERT, "piso sampu Silver"));
		expected.add(Query.newInstance(QueryType.INVALID, null));

		Assert.assertThat(actual.size(), is(3));
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void shouldConvertUnitsToSymbols() throws InvalidUnitException {
		Map<String, Unit> unitMap = DataUtil.filterUnits(data);
		String[] units = { "sampu", "piso", "lima" };
		String expected = "XIV";
		String actual = DataUtil.unitsToSymbols(units, unitMap);

		Assert.assertEquals(expected, actual);
	}
}
