package com.thoughtworks.merchantguide.util;

import java.io.File;
import java.io.FileNotFoundException;

import org.junit.Test;

public class FileUtilTest {
	final static String TEST_FILE = "test.txt";

	@Test
	public void shouldReadInputFile() throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(TEST_FILE).getFile());
		FileUtil.readInputFile(file);
	}
}
