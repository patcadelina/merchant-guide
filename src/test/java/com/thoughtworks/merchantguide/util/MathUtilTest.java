package com.thoughtworks.merchantguide.util;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;

import com.thoughtworks.merchantguide.domain.Coin;
import com.thoughtworks.merchantguide.domain.Denomination;
import com.thoughtworks.merchantguide.exception.InvalidUnitException;

public class MathUtilTest {
	@Test
	public void shouldComputeValidValue() throws InvalidUnitException {
		int actual = MathUtil.unitsToValue("MMMCMXCIX");
		int expected = 3999;

		Assert.assertEquals(expected, actual);
	}

	@Test(expected = InvalidUnitException.class)
	public void shouldThrowExceptionInvalidRepetition() throws InvalidUnitException {
		MathUtil.unitsToValue("MMMM");
	}

	@Test(expected = InvalidUnitException.class)
	public void shouldThrowExceptionInvalidSubtraction() throws InvalidUnitException {
		MathUtil.unitsToValue("IL");
	}

	@Test(expected = InvalidUnitException.class)
	public void shouldThrowExceptionInvalidOrdering() throws InvalidUnitException {
		MathUtil.unitsToValue("IVXLCDM");
	}

	@Test
	public void shouldConvertUnitToCredit() {
		Denomination denomination = Denomination.newInstance(Coin.Silver, 3, 150);
		BigDecimal expected = new BigDecimal(6).multiply(new BigDecimal(150)).divide(new BigDecimal(3));
		BigDecimal actual = MathUtil.unitToCredit(6, denomination);

		Assert.assertEquals(expected, actual);
	}
}
