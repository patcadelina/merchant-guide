package com.thoughtworks.merchantguide;

import com.thoughtworks.merchantguide.api.DataAPI;
import com.thoughtworks.merchantguide.api.QueryAPI;
import com.thoughtworks.merchantguide.repository.DenominationRepository;
import com.thoughtworks.merchantguide.repository.QueryRepository;
import com.thoughtworks.merchantguide.repository.UnitRepository;

public class Application {
	public static void main(String[] args) {
		UnitRepository unitRepository = new UnitRepository();
		DenominationRepository denominationRepository = new DenominationRepository();
		QueryRepository queryRepository = new QueryRepository();

		DataAPI dataAPI = new DataAPI();
		dataAPI.setUnitRepository(unitRepository);
		dataAPI.setDenominationRepository(denominationRepository);
		dataAPI.setQueryRepository(queryRepository);

		QueryAPI queryAPI = new QueryAPI();
		queryAPI.setQueryRepository(queryRepository);
		queryAPI.setUnitRepository(unitRepository);
		queryAPI.setDenominationRepository(denominationRepository);

		dataAPI.initData();
		queryAPI.answerQueries();
	}
}
