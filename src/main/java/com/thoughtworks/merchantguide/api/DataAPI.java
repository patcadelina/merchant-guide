package com.thoughtworks.merchantguide.api;

import java.io.File;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.merchantguide.exception.InvalidUnitException;
import com.thoughtworks.merchantguide.repository.DenominationRepository;
import com.thoughtworks.merchantguide.repository.QueryRepository;
import com.thoughtworks.merchantguide.repository.UnitRepository;
import com.thoughtworks.merchantguide.util.DataUtil;
import com.thoughtworks.merchantguide.util.FileUtil;

public class DataAPI {
	final static Logger log = LoggerFactory.getLogger(DataAPI.class);
	final static String DATA_SOURCE = "src/main/resources/input.txt";

	UnitRepository unitRepository;
	DenominationRepository denominationRepository;
	QueryRepository queryRepository;

	public void initData() {
		File file = new File(DATA_SOURCE);
		List<String> data = FileUtil.readInputFile(file);

		filterUnits(data);
		filterDenomination(data);
		filterQueries(data);
	}

	private void filterUnits(List<String> data) {
		unitRepository.setUnitMap(DataUtil.filterUnits(data));
	}

	private void filterDenomination(List<String> data) {
		try {
			denominationRepository.setDenominationMap(DataUtil.filterDenomination(data, unitRepository.getUnitMap()));
		} catch (InvalidUnitException e) {
			log.error(e.getMessage(), e);
		}
	}

	private void filterQueries(List<String> data) {
		queryRepository.setQueries(DataUtil.filterQueries(data));
	}

	public void setUnitRepository(UnitRepository unitRepository) {
		this.unitRepository = unitRepository;
	}

	public void setDenominationRepository(DenominationRepository denominationRepository) {
		this.denominationRepository = denominationRepository;
	}

	public void setQueryRepository(QueryRepository queryRepository) {
		this.queryRepository = queryRepository;
	}
}
