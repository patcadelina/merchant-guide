package com.thoughtworks.merchantguide.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.merchantguide.domain.Denomination;
import com.thoughtworks.merchantguide.domain.Query;
import com.thoughtworks.merchantguide.domain.QueryType;
import com.thoughtworks.merchantguide.domain.Unit;
import com.thoughtworks.merchantguide.exception.InvalidUnitException;
import com.thoughtworks.merchantguide.repository.DenominationRepository;
import com.thoughtworks.merchantguide.repository.QueryRepository;
import com.thoughtworks.merchantguide.repository.UnitRepository;
import com.thoughtworks.merchantguide.util.DataUtil;
import com.thoughtworks.merchantguide.util.MathUtil;

public class QueryAPI {
	final static Logger log = LoggerFactory.getLogger(QueryAPI.class);
	final static String INVALID_QUERY_RESPONSE = "I have no idea what you are talking about";
	final static String RESULT_DELIMITER = " is ";

	QueryRepository queryRepository;
	UnitRepository unitRepository;
	DenominationRepository denominationRepository;

	public void answerQueries() {
		List<Query> queries = queryRepository.getQueries();
		Map<String, Unit> unitMap = unitRepository.getUnitMap();
		try {
			for (Query query : queries) {
				if (query.getType().equals(QueryType.COMPUTE)) {
					log.info(computeQuery(query, unitMap));
				} else if (query.getType().equals(QueryType.CONVERT)) {
					log.info(convertQuery(query, unitMap));
				} else {
					log.info(INVALID_QUERY_RESPONSE);
				}
			}
		} catch (InvalidUnitException e) {
			log.error(e.getMessage(), e);
		}
	}

	private String computeQuery(Query query, Map<String, Unit> unitMap) throws InvalidUnitException {
		String symbols = DataUtil.unitsToSymbols(query.getArgs().split(" "), unitMap);
		int value = MathUtil.unitsToValue(symbols);
		StringBuilder builder = new StringBuilder();
		builder.append(query.getArgs());
		builder.append(RESULT_DELIMITER);
		builder.append(String.valueOf(value));
		return builder.toString();
	}

	private String convertQuery(Query query, Map<String, Unit> unitMap) throws InvalidUnitException {
		String[] args = query.getArgs().split(" ");
		String coinName = args[args.length - 1];
		String[] units = new String[args.length - 1];
		System.arraycopy(args, 0, units, 0, args.length - 1);
		String symbols = DataUtil.unitsToSymbols(units, unitMap);
		int unitValue = MathUtil.unitsToValue(symbols);
		Map<String, Denomination> map = denominationRepository.getDenominationMap();
		BigDecimal creditValue = MathUtil.unitToCredit(unitValue, map.get(coinName));
		StringBuilder builder = new StringBuilder();
		builder.append(query.getArgs());
		builder.append(RESULT_DELIMITER);
		builder.append(creditValue.toString());
		return builder.toString();
	}

	public void setQueryRepository(QueryRepository queryRepository) {
		this.queryRepository = queryRepository;
	}

	public UnitRepository getUnitRepository() {
		return unitRepository;
	}

	public void setUnitRepository(UnitRepository unitRepository) {
		this.unitRepository = unitRepository;
	}

	public void setDenominationRepository(DenominationRepository denominationRepository) {
		this.denominationRepository = denominationRepository;
	}
}
