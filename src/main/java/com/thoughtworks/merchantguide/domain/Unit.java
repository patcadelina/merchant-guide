package com.thoughtworks.merchantguide.domain;

public class Unit {
	String name;
	char symbol;

	public Unit(String name, char symbol) {
		this.name = name;
		this.symbol = symbol;
	}

	public static Unit newInstance(String name, char symbol) {
		return new Unit(name, symbol);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public char getSymbol() {
		return symbol;
	}

	public void setSymbol(char symbol) {
		this.symbol = symbol;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + symbol;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Unit other = (Unit) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (symbol != other.symbol)
			return false;
		return true;
	}
}
