package com.thoughtworks.merchantguide.domain;

import java.util.HashMap;
import java.util.Map;

public enum RomanNumeral {
	I(1), V(5), X(10), L(50), C(100), D(500), M(1000);

	char symbol;
	int value;

	RomanNumeral(int value) {
		this.symbol = this.name().charAt(0);
		this.value = value;
	}

	public static Map<Character, Integer> buildMap() {
		Map<Character, Integer> map = new HashMap<>();
		map.put(I.symbol, I.value);
		map.put(V.symbol, V.value);
		map.put(X.symbol, X.value);
		map.put(L.symbol, L.value);
		map.put(C.symbol, C.value);
		map.put(D.symbol, D.value);
		map.put(M.symbol, M.value);
		return map;
	}
}
