package com.thoughtworks.merchantguide.domain;

public class Denomination {
	Coin coin;
	int value;
	int credits;

	Denomination(Coin coin, int value, int credits) {
		this.coin = coin;
		this.value = value;
		this.credits = credits;
	}

	public static Denomination newInstance(Coin coin, int value, int credits) {
		return new Denomination(coin, value, credits);
	}

	public Coin getCoin() {
		return coin;
	}

	public void setCoin(Coin coin) {
		this.coin = coin;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getCredits() {
		return credits;
	}

	public void setCredits(int credits) {
		this.credits = credits;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((coin == null) ? 0 : coin.hashCode());
		result = prime * result + credits;
		result = prime * result + value;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Denomination other = (Denomination) obj;
		if (coin != other.coin)
			return false;
		if (credits != other.credits)
			return false;
		if (value != other.value)
			return false;
		return true;
	}
}
