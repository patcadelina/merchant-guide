package com.thoughtworks.merchantguide.domain;

public enum Coin {
	Silver, Gold, Iron
}
