package com.thoughtworks.merchantguide.domain;

public enum QueryType {
	COMPUTE("how much is"), CONVERT("how many Credits is"), INVALID(null);

	String structure;

	QueryType(String structure) {
		this.structure = structure;
	}

	public String getStructure() {
		return this.structure;
	}
}
