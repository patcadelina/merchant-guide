package com.thoughtworks.merchantguide.util;

import java.math.BigDecimal;
import java.util.Map;
import java.util.regex.Pattern;

import com.thoughtworks.merchantguide.domain.Denomination;
import com.thoughtworks.merchantguide.domain.RomanNumeral;
import com.thoughtworks.merchantguide.exception.InvalidUnitException;

public class MathUtil {
	final static String UNIT_COMBINATION_REGEX = "^M{0,3}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$";
	final static Map<Character, Integer> romanNumeralMap = RomanNumeral.buildMap();

	private MathUtil() {
	}

	public static int unitsToValue(String units) throws InvalidUnitException {
		if (Pattern.matches(UNIT_COMBINATION_REGEX, units)) {
			int value = 0;
			int lastNumber = 0;
			char[] numerals = units.toCharArray();
			for (int i = numerals.length - 1; i >= 0; i--) {
				int val = romanNumeralMap.get(numerals[i]);
				if (val >= lastNumber) {
					value += val;
				} else {
					value -= val;
				}
				lastNumber = val;
			}
			return value;
		} else {
			throw new InvalidUnitException();
		}
	}

	public static BigDecimal unitToCredit(int unit, Denomination denomination) {
		return new BigDecimal(unit).multiply(new BigDecimal(denomination.getCredits()))
				.divide(new BigDecimal(denomination.getValue()));
	}
}
