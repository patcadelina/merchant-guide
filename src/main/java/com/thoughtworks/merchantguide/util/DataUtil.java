package com.thoughtworks.merchantguide.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.thoughtworks.merchantguide.domain.Coin;
import com.thoughtworks.merchantguide.domain.Denomination;
import com.thoughtworks.merchantguide.domain.Query;
import com.thoughtworks.merchantguide.domain.QueryType;
import com.thoughtworks.merchantguide.domain.Unit;
import com.thoughtworks.merchantguide.exception.InvalidUnitException;

public class DataUtil {
	final static String UNIT_REGEX = "^[A-Za-z]+ is [I,V,X,L,C,D,M]$";
	final static String UNIT_SEPARATOR_REGEX = " is ";
	final static String CREDIT_REGEX = "^[A-Za-z ]+ ((Silver)|(Gold)|(Iron)) is \\d* Credits$";
	final static String CREDIT_SEPARATOR_REGEX = " ((Silver)|(Gold)|(Iron)) is ";
	final static String QUERY_REGEX = "^how ((much)|(many)) [\\w\\s]*\\?$";

	private DataUtil() {
	}

	public static Map<String, Unit> filterUnits(List<String> data) {
		Map<String, Unit> unitMap = new HashMap<>();
		for (String datum : data) {
			if (Pattern.matches(UNIT_REGEX, datum)) {
				Unit unit = parseUnit(datum);
				unitMap.put(unit.getName(), unit);
			}
		}
		return unitMap;
	}

	private static Unit parseUnit(String datum) {
		String[] unitArgs = datum.split(UNIT_SEPARATOR_REGEX);
		return Unit.newInstance(unitArgs[0], unitArgs[1].charAt(0));
	}

	public static Map<String, Denomination> filterDenomination(List<String> data, Map<String, Unit> unitMap)
			throws InvalidUnitException {
		Map<String, Denomination> denominationMap = new HashMap<>();
		for (String datum : data) {
			if (Pattern.matches(CREDIT_REGEX, datum)) {
				Denomination denomination = parseDenomination(datum, unitMap);
				denominationMap.put(denomination.getCoin().name(), denomination);
			}
		}
		return denominationMap;
	}

	private static Denomination parseDenomination(String datum, Map<String, Unit> unitMap) throws InvalidUnitException {
		String[] creditArgs = datum.split(CREDIT_SEPARATOR_REGEX);
		String[] units = creditArgs[0].split(" ");
		int value = MathUtil.unitsToValue(unitsToSymbols(units, unitMap));
		int credits = Integer.parseInt(creditArgs[1].split(" ")[0]);
		return buildDenomination(datum, value, credits);
	}

	public static String unitsToSymbols(String[] units, Map<String, Unit> unitMap) throws InvalidUnitException {
		StringBuilder builder = new StringBuilder();
		for (String unit : units) {
			if (unitMap.containsKey(unit)) {
				builder.append(unitMap.get(unit).getSymbol());
			} else {
				throw new InvalidUnitException();
			}
		}
		return builder.toString();
	}

	private static Denomination buildDenomination(String datum, int value, int credits) {
		Pattern pattern = Pattern.compile(Coin.Silver.name());
		Matcher matcher = pattern.matcher(datum);
		if (matcher.find()) {
			return Denomination.newInstance(Coin.Silver, value, credits);
		}
		pattern = Pattern.compile(Coin.Gold.name());
		matcher = pattern.matcher(datum);
		if (matcher.find()) {
			return Denomination.newInstance(Coin.Gold, value, credits);
		}
		pattern = Pattern.compile(Coin.Iron.name());
		matcher = pattern.matcher(datum);
		if (matcher.find()) {
			return Denomination.newInstance(Coin.Iron, value, credits);
		}
		return null;
	}

	public static List<Query> filterQueries(List<String> data) {
		List<Query> queries = new ArrayList<>();
		for (String datum : data) {
			if (Pattern.matches(QUERY_REGEX, datum)) {
				Query query = parseQuery(datum);
				queries.add(query);
			}
		}
		return queries;
	}

	private static Query parseQuery(String datum) {
		if (datum.startsWith(QueryType.COMPUTE.getStructure())) {
			String args = getQueryArgs(datum, QueryType.COMPUTE);
			return Query.newInstance(QueryType.COMPUTE, args);
		} else if (datum.startsWith(QueryType.CONVERT.getStructure())) {
			String args = getQueryArgs(datum, QueryType.CONVERT);
			return Query.newInstance(QueryType.CONVERT, args);
		} else {
			return Query.newInstance(QueryType.INVALID, null);
		}
	}

	private static String getQueryArgs(String datum, QueryType type) {
		int endIndex = datum.length() - 2;
		if (type.equals(QueryType.COMPUTE)) {
			int beginIndex = QueryType.COMPUTE.getStructure().length() + 1;
			return datum.substring(beginIndex, endIndex);
		} else if (type.equals(QueryType.CONVERT)) {
			int beginIndex = QueryType.CONVERT.getStructure().length() + 1;
			return datum.substring(beginIndex, endIndex);
		}
		return null;
	}
}
