package com.thoughtworks.merchantguide.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {
	final static Logger log = LoggerFactory.getLogger(FileUtil.class);
	final static Charset ENCODING = StandardCharsets.UTF_8;

	private FileUtil() {
	}

	public static List<String> readInputFile(File file) {
		List<String> data = new ArrayList<>();
		try (Scanner scanner = new Scanner(new FileInputStream(file), ENCODING.name())) {
			while (scanner.hasNextLine()) {
				data.add(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(), e);
		}
		return data;
	}
}
