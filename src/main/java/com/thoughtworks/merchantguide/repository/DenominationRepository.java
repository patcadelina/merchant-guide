package com.thoughtworks.merchantguide.repository;

import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.merchantguide.domain.Denomination;

public class DenominationRepository {
	Map<String, Denomination> denominationMap = new HashMap<>();

	public Map<String, Denomination> getDenominationMap() {
		return denominationMap;
	}

	public void setDenominationMap(Map<String, Denomination> denominationMap) {
		this.denominationMap = denominationMap;
	}
}
