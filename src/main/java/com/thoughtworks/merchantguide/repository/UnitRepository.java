package com.thoughtworks.merchantguide.repository;

import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.merchantguide.domain.Unit;

public class UnitRepository {
	Map<String, Unit> unitMap = new HashMap<>();

	public Map<String, Unit> getUnitMap() {
		return unitMap;
	}

	public void setUnitMap(Map<String, Unit> unitMap) {
		this.unitMap = unitMap;
	}
}
