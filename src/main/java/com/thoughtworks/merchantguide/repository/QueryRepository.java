package com.thoughtworks.merchantguide.repository;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.merchantguide.domain.Query;

public class QueryRepository {
	List<Query> queries = new ArrayList<>();

	public List<Query> getQueries() {
		return queries;
	}

	public void setQueries(List<Query> queries) {
		this.queries = queries;
	}
}
