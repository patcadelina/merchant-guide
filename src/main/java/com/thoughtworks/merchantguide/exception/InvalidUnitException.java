package com.thoughtworks.merchantguide.exception;

public class InvalidUnitException extends Exception {
	private static final long serialVersionUID = 7003329849477234904L;

	public InvalidUnitException() {
		super();
	}

	public InvalidUnitException(String message) {
		super(message);
	}
}
