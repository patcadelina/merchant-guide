Merchant Guide to the Galaxy
============================
This Merchant Guide to the Galaxy is a solution the the problem defined in the next section.

The main method at `src/main/java/com/thoughtworks/merchantguide/Application.java` starts the application. This application is designed as a two-tiered application. The API tier is callable from main and the Repository tier is callable from the API tier.

There are two parts to the application: data initialization and query analysis. Data is read from `src/main/resources/input.txt`, parsed and stored in the appropriate Repositories. Query analysis starts with an API call from main at which data from the first step is used.

Background
----------
You decided to give up on earth after the latest financial collapse left 99.99% of the earth's population with 0.01% of the wealth. Luckily, with the scant sum of money that is left in your account, you are able to afford to rent a spaceship, leave earth, and fly all over the galaxy to sell common metals and dirt (which apparently is worth a lot).

Buying and selling over the galaxy requires you to convert numbers and units, and you decided to write a program to help you.

The numbers used for intergalactic transactions follows similar convention to the roman numerals and you have painstakingly collected the appropriate translation between them.

Input to your program consists of lines of text detailing your notes on the conversion between intergalactic units and roman numerals.

Basic Usage
-----------
Data is read from `src/main/resources/input.txt`. If you intend to use a different set of data, please update this file and re-run.

Building
--------
Prerequisites:
* JDK 7 (or above)

To run tests:

    ./gradlew test

To build from source:

    ./gradlew build

To run merchant guide:

    java -jar build/libs/merchant-guide.jar

Author
------
Patrick Cadelina <patrickcadelina@yahoo.com>
